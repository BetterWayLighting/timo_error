#include <Arduino.h>
#include <SPI.h>

static const int cs = 17;
static const int irq = 2;

/**
 * \brief Perform a single byte command transaction with the TimoOne
 * Not all considerations have been made to account for all behaviour of the timo, but this is sufficient for reproducing the problem
 * \param command Command byte to send to the timo
 * \param tx_payload Single byte to send to the timo. For the purposes of this test we don't need more than one byte
 * \param rx_payload Pointer to a single byte to optionally receive data from the timo
 * \retval irq_flags returned from the timo after the command sequence
 */
uint8_t timo_transaction(uint8_t command, uint8_t tx_payload, uint8_t *rx_payload);

// Interrupt on falling edge of irq pin
static void irq_int();

// Flag to facilitate communication between irq_int() and regular program
static volatile bool irq_flag = false;

void setup() {
    Serial.begin(115200);
    while(!Serial);

    // Setup spi peripherial
    // You may need to change this line, since I'm using an esp32 I can specify specific pins for performing the spi transaction
    SPI.begin();

    // Setup chip select and irq pins
    pinMode(cs, OUTPUT);
    digitalWrite(cs, HIGH);
    pinMode(irq, INPUT);
    attachInterrupt(digitalPinToInterrupt(irq), irq_int, FALLING);

    Serial.println("Starting Timo Error Reproduction");
}

static void irq_int(){
    irq_flag = true;
}

uint8_t timo_transaction(uint8_t command, uint8_t tx_payload, uint8_t *rx_payload){
    digitalWrite(cs, LOW);
    delayMicroseconds(4);

    irq_flag = false;

    // Begin command transaction
    SPI.beginTransaction(SPISettings(2000000, MSBFIRST, SPI_MODE0));
    SPI.transfer(command);
    SPI.endTransaction();
    digitalWrite(cs, HIGH);

    // Wait for an irq to occur
    // This is where the program gets stuck, the irq pin never goes low again once the problem occurs
    while(!irq_flag);

    irq_flag = false;
    Serial.println("Done waiting");

    
    digitalWrite(cs, LOW);
    // Begin payload transaction
    SPI.beginTransaction(SPISettings(2000000, MSBFIRST, SPI_MODE0));
    uint8_t irq_flags;
    uint8_t rx_data;

    //SPI.transferBytes(tx_buf, rx_buf, 2);
    irq_flags = SPI.transfer(0xff);
    rx_data = SPI.transfer(tx_payload);
    SPI.endTransaction();
    digitalWrite(cs, HIGH);
    if(rx_payload){
        *rx_payload = rx_data;
    }

    // Wait for irq pin to go high
    while(!digitalRead(irq));
    delayMicroseconds(200);

    // Return irq flags
    return irq_flags;
}

void loop(){
    Serial.println("Wait for user input: ");
    // Flush serial rx
    while(Serial.available()){
        Serial.read();
    }
    // Now wait for an input
    while(!Serial.available());


    // Write config 0x83, put Timo into transit mode
    Serial.println("Write config 0x83");
    timo_transaction(0x40, 0x83, nullptr);

    // Initiate a link sequence
    // Write status 0x02
    Serial.println("Write status 0x02");
    timo_transaction(0x41, 0x02, nullptr);

    // Wait for the link procedure to finish
    Serial.println("Waiting for link procedure to complete");
    delay(8000);

    // Write dmx control 1
    Serial.println("Write dmx_control 1");
    timo_transaction(0x49, 0x01, nullptr);

    Serial.println("Wait one second");
    delay(1000);

    uint8_t status = 0;
    Serial.println("Read status");
    timo_transaction(0x01, 0, &status);
    // If the problem occurs, the status will never be printed. Try reseting your device, then notice that the first write_config() command fails too
    Serial.println("status:");
    Serial.println(status);

    // Sometimes the first run through doesn't fail, but the second time it does
    Serial.println("If you've reached here, give another user input again and the problem should occur");
}