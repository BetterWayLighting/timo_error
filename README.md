# Code to Reproduce TimoOne Error
Small script written with the Arduino framework to reproduce the error from Lumen Radio Support Request SUPPORT-2184

**Support Request: SUPPORT-2184**

This script was tested on a **Lolin D32 esp32**, a **NodeMCU esp8266** and an **Arduino Mega2560**. It might need slight adaptation to work on another board, such as setting up the spi peripheral/gpio pins, etc
This script was tested with two different TimoOnes: **{HW: 1.0.9.0, FW:0xD}** and **{HW: 1.0.10.0, FW: 0xF}**


## Sample Output Demonstrating the error

        Starting Timo Error Reproduction
        Wait for user input:
        Write config 0x83
        Done waiting
        Write status 0x02
        Done waiting
        Waiting for link procedure to complete
        Write dmx_control 1
        Done waiting       
        Wait one second    
        Read status

### How to interpret the output:

"Done waiting" is printed after the high-to-low transition on the IRQ pin after the first spi transaction of the command sequence. Notice "Done waiting" is not printed after the final read status command sequence is initiated. It will block forever here. I also encourage using a logic analyzer to directly look at the spi communication itself